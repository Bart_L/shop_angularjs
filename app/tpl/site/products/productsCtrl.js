app.controller('siteProductsCtrl', ['$scope', '$filter', '$http', 'cartFactory', function ($scope, $filter, $http, cartFactory) {

    // wywołanie zapytania http do db
    $http({
        method: 'GET',
        url: 'api/site/products/get/'
        //callback wywołujący się, kiedy połączymy się z bazą
    }).then(function successCallback(response) {
        // przypisywanie zmiennej scope do odpowiedzi z pliku
        $scope.siteproducts = response.data;
        //callback kiedy jest błąd
    }, function errorCallback(response, status, headers, config) {
        console.log('ex', response, status, headers, config);
    });

    //dodawanie produktu do koszyka po kliknięciu przycisku
    $scope.addToCart = function (product) {
        cartFactory.addItem(product);
    };

    //funckja sprawdzająca stan koszyka w widoku produktów (tak zeby na przycisku dodaj do koszyka, wyswietlala sie aktualna ilosc dodanego produktu)
    $scope.checkCart = function (product) {

        if (cartFactory.showCart().length) {

            angular.forEach(cartFactory.showCart(), function (item) {

                if (item.id == product.id) {

                    product.qty = item.qty;
                    console.log(item.qty);

                }
            });
        }
    };
}]);
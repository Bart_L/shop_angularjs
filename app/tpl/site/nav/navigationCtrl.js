/**
 * Created by Bartosz on 12.07.2017.
 */
app.controller('navigationCtrl', ['$scope', '$location', 'cartFactory', 'checkToken', '$state', function ($scope, $location, cartFactory, checkToken, $state) {
    //location.path() zwraca obecną lokalizację (adres)
    // console.log($location.path());

    $scope.navigation = function () {
        //.test testuje czy poprzedzające wyrażenie regularne występuje w adresie podanym jako argument w nawiasie
        if (/^\/admin/.test($location.path())) {

            //jeżeli użytkownik nie jest Adminem, wtedy nie wyswietlaj uzytkowników
            if (!checkToken.isAdmin()) {
                $state.go('siteproducts');
                alert('NIE JESTEŚ ADMINEM!');
            }
            return 'app/tpl/admin/nav/navigation.tpl.html';
        } else {
            if (checkToken.loggedIn()) {
                $scope.loggedIn = true;
            } else {
                $scope.loggedIn = false;
            }

            if (checkToken.isAdmin()) {
                $scope.isAdmin = true;
            } else {
                $scope.isAdmin = false;
            }
            return 'app/tpl/site/nav/navigation.tpl.html';
        }
    };

    $scope.isActive = function (path) {
        return $location.path() === path;
    };

    //funckja wylogowująca, odnosi się do servisu w ktorym czysci lokal storage
    $scope.logout = function () {
        checkToken.delToken();
        //natywna funkcja js ,przeladowanie okna po zalogowaniu ,zeby skorzystac z zapisanych ciasteczek
        location.reload();
    };

}]);
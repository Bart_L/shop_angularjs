app.controller('registerCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.user = {};

    $scope.registerSubmit = function (user) {
        $http({
            method: 'POST',
            url: 'api/site/user/create/',
            //istotne jest przekazanie nie tylko całego uzytkownika, ale też poszczególnych jego inputów po to by można się było do nich odwołać w walidacji codeigniter
            data: {
                'user': user,
                'name': user.name,
                'email': user.email,
                'password': user.password,
                'passconf': user.passconf
            }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback(errors) {

            if (errors.data) {
                $scope.errors = errors.data;
                console.log('błędy: ', $scope.errors);
            } else {
                $scope.errors = {};

                $scope.success = true;
                //wyczyszcenie inputów ze scope(zeby nie wisial niepotrzebnie na stronie )
                $scope.user = {};
                console.log('created user:', user);
            }
            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });
    };
}]);
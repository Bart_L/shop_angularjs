app.controller('myOrdersCtrl', ['$scope','$filter', '$http', function($scope, $filter, $http){

    // wywołanie zapytania http do pliku json
    $http({
        method: 'GET',
        url: 'api/site/products/get/'
        //callback wywołujący się, kiedy połączymy się z bazą
    }).then(function successCallback(response) {
        console.log('res', response);
        // przypisywanie zmiennej scope do odpowiedzi z pliku
        $scope.orders = response.data;
        console.log('users', $scope.orders);
        //callback kiedy jest błąd
    }, function errorCallback(response, status, headers, config) {
        console.log('ex', response, status, headers, config);
    });

}]);
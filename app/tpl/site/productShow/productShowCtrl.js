//stateParams pozwoli na pobranie parametrów które zostały przekazane w parametrze przeglądarki w routerze

app.controller('siteProductShowCtrl', ['$scope', '$http', '$stateParams', 'cartFactory', function ($scope, $http, $stateParams, cartFactory) {

    //console log printuje wartość id pobranego z routera po kliknięciu edycji konkretnego produktu (nazwa atrybutu po stateParams musi byc identyczna z tą określoną w state url w routerze)
    // console.log('product ID', $stateParams.id);

    var id = $stateParams.id;
    // wywołanie zapytania http do pliku json
    $http({
        method: 'GET',
        url: 'api/site/products/get/' + id
        //callback wywołujący się, kiedy połączymy się z bazą
    }).then(function successCallback(response) {
        // przypisywanie zmiennej scope do odpowiedzi z pliku , przypisuje wszystko co zostanie odesłane
        $scope.product = response.data;
        $scope.checkCart($scope.product);
        //callback kiedy jest błąd
    }, function errorCallback(response, status, headers, config) {
        console.log('ex', response, status, headers, config);
    });

    //dodawanie produktu do koszyka po kliknięciu przycisku
    $scope.addToCart = function (product) {
        cartFactory.addItem(product);
    };
    //funckja sprawdzająca stan koszyka w widoku produktów (tak zeby na przycisku dodaj do koszyka, wyswietlala sie aktualna ilosc dodanego produktu)
    $scope.checkCart = function (product) {

        if (cartFactory.showCart().length) {

            angular.forEach(cartFactory.showCart(), function (item) {
                if (item.id == product.id) {

                    product.qty = item.qty;
                    console.log(item.qty);
                }
            });
        }
    };


    function getImg() {
        //zapytanie o zdjęcia obiektu o konkretnym id
        $http({
            method: 'GET',
            url: 'api/site/images/getImages/' + id
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback(response) {
            $scope.images = response.data;
            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('ex', response);
        });
    }
    getImg();


}]);
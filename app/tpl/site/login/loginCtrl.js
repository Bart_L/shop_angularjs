app.controller('loginCtrl', ['$scope', '$http', '$state' , 'store','checkToken', function ($scope, $http, $state, store, checkToken) {

    //jezeli użytkownik jest zalogowany, przenies go do strony produktów - z użyciem state.go z ui-router
    if (checkToken.loggedIn()) {
        $state.go('siteproducts');
    }


    $scope.user = {};

    $scope.loginSubmit = function (user) {
        $http({
            method: 'POST',
            url: 'api/site/user/login/',
            //istotne jest przekazanie inputów po to by można się było do nich odwołać w walidacji codeigniter
            data: {
                'email': user.email,
                'password': user.password
            }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback(response) {

            $scope.submit = true;
            //przypisanie scope error z widoku do odpowiedzi z serwera z własciwoscią error określoną w php w funkcji login
            $scope.error = response.data.error;

            //jezeli nieprawda, że odpowiedz zawiera error , wtedy w localstorage zapisz obiekt o nazwie token zawierający odpowiedz z serwera o nazwie token ( z funkcji login w php)
            if ( !response.data.error ) {

                store.set( 'token' , response.data.token );
                location.reload();
            }

            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });




    };

}]);
app.controller('myCartCtrl', ['$scope', '$http', '$filter', 'cartFactory', 'checkToken', function ($scope, $http, $filter, cartFactory, checkToken) {


    //wchodząc na podstronę koszyka ,wywoływany jest serwis cartFactory z funkcją showCart ,co powoduje wyświetlenie zawartosci z localstorage
    $scope.cart = cartFactory.showCart();

    //funkcja czyszcząca koszyk z localstorage
    $scope.emptyCart = function () {
        cartFactory.empty();
    };

    //funckja zwracająca końcową cenę koszyka, iteracja po tabeli cart
    $scope.totalPrice = function () {
        var total = 0;

        angular.forEach($scope.cart, function (item) {
            total += item.qty * item.price;
        });
        //zanim zostanie zwrócony total, zaokrąglamy go do 2 m. po przecinku
        total = $filter('number')(total, 2);

        return total;
    };


    $scope.removeCartItem = function ($index) {
        //usuwanie z widoku
        $scope.cart.splice($index, 1);
        //usuwanie z serwisu
        cartFactory.update($scope.cart);
    };

    $scope.setOrder = function ($event) {
        //prevent default zeby formularz zachowywał się tak jak to zaprogramowane, a nie domyslnie
        $event.preventDefault();


        //sprawdz czy użytkownik jest zalogowany używając tokena
        if (!checkToken.loggedIn()) {
            $scope.alert = {type: 'warning', msg: 'Musisz być zalogowany!'};
            return false;
        }

        $http({
            method: 'POST',
            url: 'api/site/orders/create/',
            data: {
                'token': checkToken.rawToken(),
                'payload': checkToken.payload(),
                'items': $scope.cart,
                'total': $scope.totalPrice()
            }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback(data) {
            // czyszczenie koszyka po złożeniu zamówienia
            cartFactory.empty();
            //alert wyświetlający sukces zakupów
            $scope.alert = {type: 'success', msg: 'NIE ODŚWIERZAJ STRONY! Trwa przekierowanie do płatności...'};
            //uruchomienie formularza Paypal za pomocą jQuery
            $('#paypalForm').submit();
        //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });
    };

    //obserwowanie zmian koszyka po zmianie np. ilosci
    $scope.$watch(function () {
        cartFactory.update($scope.cart);
    });
}]);
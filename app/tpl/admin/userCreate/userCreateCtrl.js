app.controller('userCreateCtrl', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    //ustawienie domyślnej roli dla tworzonego użytkownika na user
    $scope.user = {};
    $scope.user.role = 'user';

    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
    $scope.createUser = function (user) {
        $http({
            method: 'POST',
            url: 'api/admin/users/create/',
            //istotne jest przekazanie nie tylko całego uzytkownika, ale też poszczególnych jego inputów po to by można się było do nich odwołać w walidacji codeigniter
            data: {
                'user': user,
                'name': user.name,
                'email': user.email,
                'password': user.password,
                'passconf': user.passconf
            }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback(errors) {
            console.log('err', errors);

            if (errors.data) {
                $scope.errors = errors.data;
                console.log('błędy: ' , $scope.errors);
            } else {
                $scope.success = true;
                console.log('OK!');
                //angularowy timeout uzyty tutaj w celu przypisania do powyzszego success wartosci false po 3 sekundach, co spowoduje zniknięcie alertu o sukcesie po trzech sekundach , a także wyczyszcenie inputów ze scope(zeby nie wisial niepotrzebnie na stronie )
                $timeout(function () {
                    $scope.success = false;
                    $scope.user = {};
                }, 2500);

                console.log('created user:', user);
            }
            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });
    };
}]);
//stateParams pozwoli na pobranie parametrów które zostały przekazane w parametrze przeglądarki w routerze

app.controller('productEditCtrl', ['$scope','$http','$stateParams', 'FileUploader', '$timeout', function($scope, $http, $stateParams, FileUploader ,$timeout){

    //console log printuje wartość id pobranego z routera po kliknięciu edycji konkretnego produktu (nazwa atrybutu po stateParams musi byc identyczna z tą określoną w state url w routerze)
    console.log( 'params ID', $stateParams.id);


    var productId = $stateParams.id;
    $scope.id = productId;
    // wywołanie zapytania http do db
    $http({
        method: 'GET',
        url: 'api/admin/products/get/' + productId
        //callback wywołujący się, kiedy połączymy się z bazą
    }).then(function successCallback(response) {
        console.log('res', response);
        // przypisywanie zmiennej scope do odpowiedzi z pliku , przypisuje wszystko co zostanie odesłane
        $scope.product = response.data;

        //callback kiedy jest błąd
    }, function errorCallback(response, status, headers, config) {
        console.log('ex', response, status, headers, config);
    });

    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
    $scope.saveChanges = function(product) {

        $http({
            method: 'POST',
            url: 'api/admin/products/update/' + productId,
            data: { 'product' : product }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback() {
            $scope.success = true;

            //angularowy timeout uzyty tutaj w celu przypisania do powyzszego success wartosci false po 3 sekundach, co spowoduje zniknięcie alertu o sukcesie po trzech sekundach (zeby nie wisial niepotrzebnie na stronie )
            $timeout(function () {
                $scope.success = false;
            }, 2500);
            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });

        console.log('updated object:', product);
    };


    function getImg() {
        //zapytanie o zdjęcia obiektu o konkretnym id
        $http({
            method: 'GET',
            url: 'api/admin/images/getImages/' + productId
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback(response) {
            console.log('images', response.data);
            $scope.images = response.data;
            //callback kiedy jest błąd
        }, function errorCallback(response, status, headers, config) {
        });
    }
    getImg();

    var uploader = $scope.uploader = new FileUploader({
        url: 'api/admin/images/upload/' +  productId // sciezka do ścieżki wyświetlanej w adresie przegladarki (w funkcji upload  w phpowym kontrollerze images.php)
    });

    // FILTERS dla uploadera

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    //dodanie funkcji getImg kiedy akcja jest complete, sluzy do tego zeby zdjecia pokazywaly sie od razu po uploadzie a nie po odsiwzeniu
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
        getImg();
    };

    $scope.delImg = function (image, $index) {

        //usuwamy z widoku obrazek za pomocą indexu przekazywanego w funkcji
        $scope.images.splice( $index, 1 );

        $http({
            method: 'POST',
            url: 'api/admin/images/delImages/',
            data: { 'id': productId, 'image': image }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback() {
            //UWAGA!!! zamiast używania powyższego splice na images w celu odświeżenia widoku, możemy usunąć powyższy splice, a tutaj w successCallback przywolac funkcjję getImg();
            //callback kiedy jest błąd
        }, function errorCallback() {
        });

    };
}]);
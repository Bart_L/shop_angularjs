//stateParams pozwoli na pobranie parametrów które zostały przekazane w parametrze przeglądarki w routerze

app.controller('userEditCtrl', ['$scope', '$http', '$stateParams', '$timeout', function ($scope, $http, $stateParams, $timeout) {

    //console log printuje wartość id pobranego z routera po kliknięciu edycji konkretnego produktu (nazwa atrybutu po stateParams musi byc identyczna z tą określoną w state url w routerze)
    console.log('params ID', $stateParams.id);


    var userId = $stateParams.id;
    $scope.id = userId;

    // wywołanie zapytania http do db
    $http({
        method: 'GET',
        url: 'api/admin/users/get/' + userId
        //callback wywołujący się, kiedy połączymy się z bazą
    }).then(function successCallback(response) {
        console.log('res', response);
        // przypisywanie zmiennej scope do odpowiedzi z pliku , przypisuje wszystko co zostanie odesłane
        $scope.user = response.data;

        //callback kiedy jest błąd
    }, function errorCallback(response, status, headers, config) {
        console.log('ex', response, status, headers, config);
    });


    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
    $scope.saveUserChanges = function (user) {

        $http({
            method: 'POST',
            url: 'api/admin/users/update/',
            //istotne jest przekazanie nie tylko całego uzytkownika, ale też poszczególnych jego inputów po to by można się było do nich odwołać w walidacji codeigniter
            data: {
                'user': user,
                'id': userId,
                'name': user.name,
                'email': user.email,
                'password': user.password,
                'passconf': user.passconf
            }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback(errors) {
            if (errors.data) {

                $scope.errors = errors.data;
                console.log('błędy: ', $scope.errors);
            } else {
                $scope.success = true;
                console.log('OK!');
                //angularowy timeout uzyty tutaj w celu przypisania do powyzszego success wartosci false po 3 sekundach, co spowoduje zniknięcie alertu o sukcesie po trzech sekundach , a także wyczyszcenie inputów ze scope(zeby nie wisial niepotrzebnie na stronie )
                $timeout(function () {
                    $scope.success = false;
                    $scope.user = {};
                }, 2500);

                console.log('created user:', user);
            }
            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });
        console.log('updated user:', user);
    };


}]);
app.controller('productsCtrl', ['$scope', '$filter', '$http', function ($scope, $filter, $http) {

    // wywołanie zapytania http do database
    $http({
        method: 'GET',
        url: 'api/admin/products/get'
        //callback wywołujący się, kiedy połączymy się z bazą
    }).then(function successCallback(response) {
        console.log('res', response);
        // przypisywanie zmiennej scope do odpowiedzi z pliku
        $scope.products = response.data;
        console.log('products', $scope.products);
        //callback kiedy jest błąd
    }, function errorCallback(response, status, headers, config) {
        console.log('ex', response, status, headers, config);
    });

    $scope.deleteProduct = function (product, $index) {

        //jeżeli admin nie potwierdzi usuwania, to skrypt poniżej zostanie zatrzymany
        if (!confirm('Czy na pewno chcesz usunąć?'))
            return false;
        // spliceowanie tablicy param 1 - index usuwanego obiektu; param 2 - ile elementów od tego elementu usunąć; param 3 - jezeli w drugim podamy 0 to znaczy ze nie usuwamy i jako trzeci mozemy wstawic nowy obiekt wstawiany za obecny np. {nowy}
        $scope.products.splice($index, 1);


        $http({
            method: 'POST',
            url: 'api/admin/products/delete/',
            data: { 'product' : product }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback() {
            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });

        console.log('usunięto obiekt: ', product, 'o indeksie: ', $index);

    };

}]);

app.controller('productCreateCtrl', ['$scope','$http', '$timeout', function($scope, $http, $timeout){


    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
    $scope.createProduct = function(product) {
        $http({
            method: 'POST',
            url: 'api/admin/products/create/',
            data: { 'product' : product }
            //callback wywołujący się, kiedy połączymy się z bazą
        }).then(function successCallback() {
            $scope.success = true;

            //angularowy timeout uzyty tutaj w celu przypisania do powyzszego success wartosci false po 3 sekundach, co spowoduje zniknięcie alertu o sukcesie po trzech sekundach , a także wyczyszcenie inputów ze scope(zeby nie wisial niepotrzebnie na stronie )
            $timeout(function () {
                $scope.success = false;
                $scope.product = [];

            }, 2500);
            //callback kiedy jest błąd
        }, function errorCallback(response) {
            console.log('Communication Error!', response);
        });

        console.log('created object:', product);
    };

}]);
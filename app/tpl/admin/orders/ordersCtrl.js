app.controller('ordersCtrl', ['$scope','$filter', '$http', function($scope, $filter, $http){

    // wywołanie zapytania http do pliku json
    $http({
        method: 'GET',
        url: 'app/model/orders.json'
        //callback wywołujący się, kiedy połączymy się z bazą
    }).then(function successCallback(response) {
        console.log('res', response);
        // przypisywanie zmiennej scope do odpowiedzi z pliku
        $scope.orders = response.data;
        console.log('users', $scope.orders);
        //callback kiedy jest błąd
    }, function errorCallback(response, status, headers, config) {
        console.log('ex', response, status, headers, config);
    });

    //funkcja powoduje zmiane statusu z 0 na 1 , czyli z oczekującego na wysłane po kliku
        $scope.changeStatus = function(order) {

        console.log('before', order.status);

        if(order.status == !order.status ) {
            order.status = order.status;
        } else {
            order.status = !order.status;
        }
        console.log('after', order.status);

        // TODO: usuwanie za pomocą API

    };

    $scope.deleteOrder = function(user, $index) {
        //jeżeli admin nie potwierdzi usuwania, to skrypt poniżej zostanie zatrzymany
        if (!confirm('Czy na pewno chcesz usunąć?'))
            return false;
        // spliceowanie tablicy param 1 - index usuwanego obiektu; param 2 - ile elementów od tego elementu usunąć; param 3 - jezeli w drugim podamy 0 to znaczy ze nie usuwamy i jako trzeci mozemy wstawic nowy obiekt wstawiany za obecny np. {nowy}
        $scope.orders.splice($index, 1);
        console.log('usunięto zamówienie: ', user, 'o indexie: ', $index);

        // TODO: usuwanie za pomocą API

    };
}]);
'use strict';
var app = angular.module('appShop', ['ui.router', 'angular-storage', 'angularFileUpload', 'angular-jwt']);

app.config(['$urlRouterProvider', '$stateProvider', '$httpProvider', function ($urlRouterProvider, $stateProvider, $httpProvider) {

    //router przekierowujący do home, jeżeli nie jest wpisany inny adres
    $urlRouterProvider.otherwise('/home');

    //routing do poszczególnych stron (url, template i obsługujący kontrollers)
    $stateProvider
        //==========================LOGIN AND REGISTER
        .state('login', {
            url: '/login',
            templateUrl: 'app/tpl/site/login/login.tpl.html',
            controller: 'loginCtrl'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'app/tpl/site/register/register.tpl.html',
            controller: 'registerCtrl'
        })
        //=========================ADMIN PRODUCTS
        .state('products', {
            url: '/admin/products',
            templateUrl: 'app/tpl/admin/products/products.tpl.html',
            controller: 'productsCtrl'
        })
        .state('productedit', {
            //użyty tutaj został $httpProvider do przypisania id pojedynczego produktu do adresu
            url: '/admin/product/edit/:id',
            templateUrl: 'app/tpl/admin/productEdit/productEdit.tpl.html',
            controller: 'productEditCtrl'
        })
        .state('productcreate', {
            url: '/admin/product/create',
            templateUrl: 'app/tpl/admin/productsCreate/productCreate.tpl.html',
            controller: 'productCreateCtrl'
        })
        //======================== ADMIN USERS
        .state('users', {
            url: '/admin/users',
            templateUrl: 'app/tpl/admin/users/users.tpl.html',
            controller: 'usersCtrl'
        })
        .state('useredit', {
            //użyty tutaj został $httpProvider do przypisania id pojedynczego uzytkownika do adresu
            url: '/admin/user/edit/:id',
            templateUrl: 'app/tpl/admin/userEdit/userEdit.tpl.html',
            controller: 'userEditCtrl'
        })
        .state('usercreate', {
            url: '/admin/user/create',
            templateUrl: 'app/tpl/admin/userCreate/userCreate.tpl.html',
            controller: 'userCreateCtrl'
        })
        //========================ADMIN ORDERS
        .state('orders', {
            url: '/admin/orders',
            templateUrl: 'app/tpl/admin/orders/orders.tpl.html',
            controller: 'ordersCtrl'
        })
        //========================SITE ORDERS - user view
        .state('myorders', {
            url: '/site/myorders',
            templateUrl: 'app/tpl/site/myOrders/myOrders.tpl.html',
            controller: 'myOrdersCtrl'
        })
        .state('siteproducts', {
            url: '/products',
            templateUrl: 'app/tpl/site/products/products.tpl.html',
            controller: 'siteProductsCtrl'
        })
        .state('siteproduct', {
            //użyty tutaj został $httpProvider do przypisania id pojedynczego produktu do adresu
            url: '/product/:id',
            templateUrl: 'app/tpl/site/productShow/productShow.tpl.html',
            controller: 'siteProductShowCtrl'
        })
        .state('mycart', {
            url: '/mycart',
            templateUrl: 'app/tpl/site/mycart/mycart.tpl.html',
            controller: 'myCartCtrl'
        });

}]);
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends CI_Controller
{

    public function upload($id)
    {
        //printuje scieżkę bezwzględną o pliku, którą podamy niżej w uploadPath
        //        echo FCPATH;


        //if przeklejony z pliku upload.php dołączonego do paczki w angular-file-upload
        if (!empty($_FILES)) {

            $tempPath = $_FILES['file']['tmp_name'];

            //scieżka bezwzględna do folderu uploads w folderze app
            $basePath = FCPATH . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;

            //do ścieżki dodaje $id przekazywane z funkcją upload, aby zdjecia uploadowaly sie do podwolderów z id produktu a nie wszystkie do jednego
            $basePath = $basePath . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR;

            //utworzenie folderu w powyżej lokalizacji dla zdjęcia danego produktu (mkdir przyjmuje dwa param. 1. scieżke, 2. parametr dot. określenia czy zawartośc jest read czy write 0700 - to odczyt 0777 wszystkie prawa
            mkdir($basePath, 0700);

            //ostateczna scieżka to powyższa scieżka z id produktu + nazwa pliku
            $uploadPath = $basePath . $_FILES['file']['name'];

            //ze ścieżki tymczasowej plik jest przenoszony na stałe do uploadpath
            move_uploaded_file($tempPath, $uploadPath);

            $answer = array('answer' => 'File transfer completed');
            $json = json_encode($answer);

            echo $json;

        } else {

            echo 'No files';

        }
    }

    public function getImages($id)
    {
        $basePath = FCPATH . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
        $basePath = $basePath .  $id . DIRECTORY_SEPARATOR;

        //jeżeli ścieżka nie jest folderem, wtedy nie skanuj
        if (!is_dir($basePath))
            return;

        //żeby wyświetlić zdjęcia, musimy przeskanować folder w poszukiwaniu zdjęć
        $files = scandir($basePath);

        //żeby usunąć z tablicy files dwa pierwsze rekordy (czyli kropki które służyły za oznaczenie powrotu folderów) używamy funkcji php array_diff 1. argument to nazwa tablicy z ktorej usuwamy, 2. argument to tablica z obiektami ktore chcemy usunac
        $files = array_diff($files, array('.', '..'));

        //reset kluczy - zeby pozostale elementy po usunięciu poprzednich uzyskaly indexy odpowiednie
        $newFiles = array();

        foreach ($files as $file) {
            $newFiles[] .= $file;
        }
        //var_dump to inny rodzaj printowania, printuje tez tablice (printuje wszystko) / pre zwiekszają czytelnosc obiektu
//        echo '<pre>';
//        var_dump($newFiles);
//        echo '</pre>';

        //zakodowanie tablicy do formatu json i wyprintowanie ich na ekran
        echo json_encode($newFiles);
    }

    public function delImages()
    {
        //tworzymy zmienną post do odbierania danych z formularza delImages

        $post = file_get_contents('php://input');
        var_dump($post);

        //przekazując drugi parametr na true, dekodujemy nie jako obiekt tylko tablica
        $_POST = json_decode($post, true);
        //teraz printowanie odbiera ciąg znakow odpowiadający za obrazek

        //musimy zdefinować id , przypiszemy do niego pobranie z formularza ; w codeigniter this odwoluje sie do codeigniter i pobieranie odbywa sie za pomocą input post; w nawiasie parametr to pole z jakiego bedziemy pobierac
        $id = $this->input->post('id');
        //kolejny element to image , definiujemy id obrazka
        $image = $this->input->post('image');

        $imagePath = FCPATH . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
        $imagePath = $imagePath . $id . DIRECTORY_SEPARATOR;
        //poza ścieżką potrzebna jeszcze nazwa obrazka wiec do imagepath dodajemy nazwe
        $imagePath = $imagePath . $image;

        unlink($imagePath);


    }
}

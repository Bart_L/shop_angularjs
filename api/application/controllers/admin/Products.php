<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    // w construct wszystkie funckje są dostępne na zewnątrz (podobnie jak rootscope w angularzejs
    public function __construct()
    {
        parent::__construct();
        //odbieranie inputów z angulara
        $post = file_get_contents('php://input');
        //przekazując drugi parametr na true, dekodujemy nie jako obiekt tylko tablica
        $_POST = json_decode($post, true);

        //zaladowanie modelu danych do funkcji (w tym przypadku modelu z folderu models/admin w api), model ten wczytuje dane o produktach z bazy i jest dostępny globalnie z racji umieszczenia w construct
        $this->load->model('admin/Products_model');
    }

    public function get($id = false)
    {
        //po zaladowaniu modelu, przypisujemy do zmiennej $output funkcje get modelu (nie kontrolera, tylko modelu o tej samej nazwie get)
        $output = $this->Products_model->get($id);
        //zakodowanie danych pobranych z modelu za pomocą get i przypisanych do output do JSONa
        echo json_encode( $output );
    }
    //funkcja updatująca konkretny element product do bazy

    public function update()
    {
        //dane które pobieramy z inputa nazywają sie product, poniewaz tak nazwaliśmy je w angularze w http post funkcji SaveChanges (przypisane do data) - w productEditCtrl
        $product = $this->input->post('product');
        //zwroci nazwe updateowanego produktu
        echo $product['name'];
        //wywołanie funkcji update, która jest napisana w modelu Products_model ; przekazujemy $product który odbieramy pozniej w modelu
        $this->Products_model->update($product);


    }

    //funkcja zapisująca nowy element product do bazy
    public function create()
    {
        //dane które pobieramy z inputa nazywają sie product, poniewaz tak nazwaliśmy je w angularze w http post funkcji createProduct (przypisane do data) - w productCreateCtrl
        $product = $this->input->post('product');
        //zwroci nazwe tworzonego  produktu
        echo $product['name'];
        //wywołanie funkcji create, która jest napisana w modelu Products_model ; przekazujemy $product który odbieramy pozniej w modelu
        $this->Products_model->create($product);
    }

    //funkcja usuwajaca element product z bazy
    public function delete()
    {
        //dane które pobieramy z inputa nazywają sie product, poniewaz tak nazwaliśmy je w angularze w http post funkcji createProduct (przypisane do data) - w productCreateCtrl
        $product = $this->input->post('product');

        //wywołanie funkcji delete, która jest napisana w modelu Products_model ; przekazujemy $product który odbieramy pozniej w modelu
        $this->Products_model->delete($product);
    }
}



<?php
/**
 * Created by PhpStorm.
 * User: 7comp
 * Date: 2017-07-18
 * Time: 11:31
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{

    // w construct wszystkie funckje są dostępne na zewnątrz (podobnie jak rootscope w angularzejs
    public function __construct()
    {
        parent::__construct();
        //odbieranie inputów z angulara
        $post = file_get_contents('php://input');
        //przekazując drugi parametr na true, dekodujemy nie jako obiekt tylko tablica
        $_POST = json_decode($post, true);

        //zaladowanie modelu danych do funkcji (w tym przypadku modelu z folderu models/admin w api), model ten wczytuje dane o produktach z bazy i jest dostępny globalnie z racji umieszczenia w construct
        $this->load->model('admin/Users_model');
    }

    public function get($id = false)
    {
        //po zaladowaniu modelu, przypisujemy do zmiennej $output funkcje get modelu (nie kontrolera, tylko modelu o tej samej nazwie get)
        $output = $this->Users_model->get($id);
        //zakodowanie danych pobranych z modelu za pomocą get i przypisanych do output do JSONa
        echo json_encode($output);
    }

    //funkcja updatująca konkretny element User do bazy

    public function update()
    {

        //ustawienia wyswietlania powiadomien delimiterów (zeby w errorach nie wystwietlaly sie znaczki z tagów htmlowych)
        $this->form_validation->set_error_delimiters('', '');
        //zdefinowanie zasad dla walidacji do formularza (argumenty: 1 - pole, 2- przypisywanie nazwy do pola 3 - forma walidacji - kolene formy dodajemy do jednego pola po pionowej linii np 'required|cośtam')
        $this->form_validation->set_rules('name', 'Imię', 'required|min_length[3]');
        //tu walidacja email najpierw wymaga podania adresu , potem sprawdza czy wartosc wpisana ma poprawny format, a na koncu własna funkcja callback
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_unique_email');
        //hasło takie samo jak passconf
        $this->form_validation->set_rules('password', 'Nowe Hasło', 'matches[passconf]');
        //potwierdzenie takie samo jak password
        $this->form_validation->set_rules('passconf', 'Powtórz Nowe hasło', 'matches[password]');


        //na początku koniecznie jest sprawdzenie czy w bazie nie ma już takiego samego użytkownika (nie chcemy powielać tych samych mailów)
        //UWAGA!!!!!!!!!!!!!!!!!!!1 by załadować liba z walidacją codeignitera, musimy w pliku autoload.php dodać form_validation do libraries

        // if sprawdzający validację przesłanego formularza
        if ($this->form_validation->run()) {
            //dane które pobieramy z inputa nazywają sie User, poniewaz tak nazwaliśmy je w angularze w http post funkcji SaveChanges (przypisane do data) - w UserEditCtrl
            $user = $this->input->post('user');
            //zwroci nazwe updateowanego produktu
//            echo $user['name'];

            //przypisywanie do hasła funkcji crypt szufrującą hasło 2 arg - pierszy to haslo, drugi sół (w tym wypadku encrytpion key z config.php)
            $user['password'] = crypt($user['password'], config_item('encryption_key'));

            //odpinanie, żeby nie było przesyłane do bazy, ma być tylko przesyłane hasło, a nie potwierdzenie hasła
            unset($user['passconf']);
            //wywołanie funkcji update, która jest napisana w modelu Users_model ; przekazujemy $User który odbieramy pozniej w modelu
            $this->Users_model->update($user);
        } else {
            //outup przyjmuje tablice o nazwie name (taka jak podana powyżej w set_rules), nastepnie przypisywany jest do form_error które jako argument bierze pole z Angulara z data
            $errors['name'] = form_error('name');
            $errors['email'] = form_error('email');
            $errors['password'] = form_error('password');
            $errors['passconf'] = form_error('passconf');
            echo json_encode($errors);
        }


    }

    //funkcja zapisująca nowy element User do bazy
    public function create()
    {
        //ustawienia wyswietlania powiadomien delimiterów (zeby w errorach nie wystwietlaly sie znaczki z tagów htmlowych)
        $this->form_validation->set_error_delimiters('', '');
        //zdefinowanie zasad dla walidacji do formularza (argumenty: 1 - pole, 2- przypisywanie nazwy do pola 3 - forma walidacji - kolene formy dodajemy do jednego pola po pionowej linii np 'required|cośtam')
        $this->form_validation->set_rules('name', 'Imię', 'required|min_length[3]');
        //tu walidacja email najpierw wymaga podania adresu , potem sprawdza czy wartosc wpisana ma poprawny format, a na koncu sprawdza czy adres email jest unikalny [tabela.kolumna]
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        //hasło wymagane i takie samo jak passconf
        $this->form_validation->set_rules('password', 'Hasło', 'required|matches[passconf]');
        //potwierdzenie wymagane i takie samo jak password
        $this->form_validation->set_rules('passconf', 'Powtórz hasło', 'required|matches[password]');


        //na początku koniecznie jest sprawdzenie czy w bazie nie ma już takiego samego użytkownika (nie chcemy powielać tych samych mailów)
        //UWAGA!!!!!!!!!!!!!!!!!!!1 by załadować liba z walidacją codeignitera, musimy w pliku autoload.php dodać form_validation do libraries

        // if sprawdzający validację przesłanego formularza
        if ($this->form_validation->run()) {

            //dane które pobieramy z inputa nazywają sie User, poniewaz tak nazwaliśmy je w angularze w http post funkcji createUser (przypisane do data) - w UserCreateCtrl
            $user = $this->input->post('user');
            //zwroci nazwe tworzonego  produktu
            //echo $user['name'];

            //przypisywanie do hasła funkcji crypt szufrującą hasło 2 arg - pierszy to haslo, drugi sół (w tym wypadku encrytpion key z config.php)
            $user['password'] = crypt($user['password'], config_item('encryption_key'));

            //odpinanie, żeby nie było przesyłane do bazy, ma być tylko przesyłane hasło, a nie potwierdzenie hasła
            unset($user['passconf']);
            //wywołanie funkcji create, która jest napisana w modelu Users_model ; przekazujemy $User który odbieramy pozniej w modelu
            $this->Users_model->create($user);
        } else {
            //outup przyjmuje tablice o nazwie name (taka jak podana powyżej w set_rules), nastepnie przypisywany jest do form_error które jako argument bierze pole z Angulara z data
            $errors['name'] = form_error('name');
            $errors['email'] = form_error('email');
            $errors['password'] = form_error('password');
            $errors['passconf'] = form_error('passconf');
            echo json_encode($errors);
        }

    }

    //funkcja usuwajaca element User z bazy
    public function delete()
    {
        //dane które pobieramy z inputa nazywają sie User, poniewaz tak nazwaliśmy je w angularze w http post funkcji createUser (przypisane do data) - w UserCreateCtrl
        $user = $this->input->post('user');

        //wywołanie funkcji delete, która jest napisana w modelu Users_model ; przekazujemy $User który odbieramy pozniej w modelu
        $this->Users_model->delete($user);
    }

    function unique_email()
    {
        $id = $this->input->post('id');
        $email = $this->input->post('email');

        if ( $this->Users_model->get_unique($id, $email))
        {
            $this->form_validation->set_message('unique_email', 'inny użytkownik ma taki email!');
            return false;
        } else
        {
            return true;

        }
    }
}
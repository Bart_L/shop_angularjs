<?php
/**
 * Created by PhpStorm.
 * User: Bartosz
 * Date: 19.07.2017
 * Time: 19:52
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    // w construct wszystkie funckje są dostępne na zewnątrz (podobnie jak rootscope w angularzejs
    public function __construct()
    {
        parent::__construct();
        //odbieranie inputów z angulara
        $post = file_get_contents('php://input');
        //przekazując drugi parametr na true, dekodujemy nie jako obiekt tylko tablica
        $_POST = json_decode($post, true);

        //zaladowanie modelu danych do funkcji (w tym przypadku modelu z folderu models/admin w api), model ten wczytuje dane o produktach z bazy i jest dostępny globalnie z racji umieszczenia w construct
        $this->load->model('site/User_model');
    }

    public function get($id)
    {
        //po zaladowaniu modelu, przypisujemy do zmiennej $output funkcje get modelu (nie kontrolera, tylko modelu o tej samej nazwie get)
        $output = $this->Users_model->get($id);
        //zakodowanie danych pobranych z modelu za pomocą get i przypisanych do output do JSONa
        echo json_encode($output);
    }

    //funkcja updatująca konkretny element User do bazy


    //funkcja zapisująca nowy element User do bazy
    public function create()
    {
        //ustawienia wyswietlania powiadomien delimiterów (zeby w errorach nie wystwietlaly sie znaczki z tagów htmlowych)
        $this->form_validation->set_error_delimiters('', '');
        //zdefinowanie zasad dla walidacji do formularza (argumenty: 1 - pole, 2- przypisywanie nazwy do pola 3 - forma walidacji - kolene formy dodajemy do jednego pola po pionowej linii np 'required|cośtam')
        $this->form_validation->set_rules('name', 'Imię', 'required|min_length[3]');
        //tu walidacja email najpierw wymaga podania adresu , potem sprawdza czy wartosc wpisana ma poprawny format, a na koncu sprawdza czy adres email jest unikalny [tabela.kolumna]
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        //hasło wymagane i takie samo jak passconf
        $this->form_validation->set_rules('password', 'Hasło', 'required|matches[passconf]');
        //potwierdzenie wymagane i takie samo jak password
        $this->form_validation->set_rules('passconf', 'Powtórz hasło', 'required|matches[password]');


        //na początku koniecznie jest sprawdzenie czy w bazie nie ma już takiego samego użytkownika (nie chcemy powielać tych samych mailów)
        //UWAGA!!!!!!!!!!!!!!!!!!!1 by załadować liba z walidacją codeignitera, musimy w pliku autoload.php dodać form_validation do libraries

        // if sprawdzający validację przesłanego formularza
        if ($this->form_validation->run()) {

            //dane które pobieramy z inputa nazywają sie User, poniewaz tak nazwaliśmy je w angularze w http post funkcji createUser (przypisane do data) - w UserCreateCtrl
            $user = $this->input->post('user');
            //zwroci nazwe tworzonego  produktu
            //echo $user['name'];

            //przypisywanie role jako 'user' domyslnie przy zakladaniu konta, aby uzytownik nie mogl sie zarejestrowac jako admin
            $user['role'] = 'user';


            //przypisywanie do hasła funkcji crypt szufrującą hasło 2 arg - pierszy to haslo, drugi sół (w tym wypadku encrytpion key z config.php)
            $user['password'] = crypt($user['password'], config_item('encryption_key'));

            //odpinanie, żeby nie było przesyłane do bazy, ma być tylko przesyłane hasło, a nie potwierdzenie hasła
            unset($user['passconf']);
            //wywołanie funkcji create, która jest napisana w modelu Users_model ; przekazujemy $User który odbieramy pozniej w modelu
            $this->User_model->create($user);
        } else {
            //outup przyjmuje tablice o nazwie name (taka jak podana powyżej w set_rules), nastepnie przypisywany jest do form_error które jako argument bierze pole z Angulara z data
            $errors['name'] = form_error('name');
            $errors['email'] = form_error('email');
            $errors['password'] = form_error('password');
            $errors['passconf'] = form_error('passconf');
            echo json_encode($errors);
        }

    }

    public function login()
    {

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password = crypt( $password , config_item( 'encryption_key' ) );

        $login = $this->User_model->login($email, $password);

        //jesli zmienna login zawierac bedzie error z funkcji modelu login, wtedy output wyswietli error z wiadomoscią
        if (!$login) {
            $output['error'] = 'Błędne hasło lub email';
        } //jeżeli ok, wtedy tworzymy token

        else {
            //tworzymy nową biblioteke jwt.php w folderze libraries  i tu z  niej korzystamy, należy pamietac o zaladowaniu jej w autoload.php
            //tworzymy token za pomocą jwt, encode wymaga 2 param - 1. tabela, czyli config tokena i co zostaje w nim przekazane 2. klucz (klucz użyty z encryption_key podanym w config.php)

            $token = $this->jwt->encode(array(
                'userId' => $login->id,
                'name' => $login->name,
                'email' => $login->email,
                'role' => $login->role
            ), config_item('encryption_key'));

            //wyjsciowy output z warotsciami dodawanymi z serwera w modelu User_model, nadpisywany jest tokenem
            $output['token'] = $token;
        }

        //dekodowanie outputu do jsona
        echo json_encode($output);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: 7comp
 * Date: 2017-07-19
 * Time: 14:40
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends CI_Controller
{

    public function getImages($id)
    {
        $basePath = FCPATH . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
        $basePath = $basePath .  $id . DIRECTORY_SEPARATOR;

        //jeżeli ścieżka nie jest folderem, wtedy nie skanuj
        if (!is_dir($basePath))
            return;

        //żeby wyświetlić zdjęcia, musimy przeskanować folder w poszukiwaniu zdjęć
        $files = scandir($basePath);

        //żeby usunąć z tablicy files dwa pierwsze rekordy (czyli kropki które służyły za oznaczenie powrotu folderów) używamy funkcji php array_diff 1. argument to nazwa tablicy z ktorej usuwamy, 2. argument to tablica z obiektami ktore chcemy usunac
        $files = array_diff($files, array('.', '..'));

        //reset kluczy - zeby pozostale elementy po usunięciu poprzednich uzyskaly indexy odpowiednie
        $newFiles = array();

        foreach ($files as $file) {
            $newFiles[] .= $file;
        }
        //var_dump to inny rodzaj printowania, printuje tez tablice (printuje wszystko) / pre zwiekszają czytelnosc obiektu
//        echo '<pre>';
//        var_dump($newFiles);
//        echo '</pre>';

        //zakodowanie tablicy do formatu json i wyprintowanie ich na ekran
        echo json_encode($newFiles);
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: 7comp
 * Date: 2017-07-26
 * Time: 09:37
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    // w construct wszystkie funckje są dostępne na zewnątrz (podobnie jak rootscope w angularzejs
    public function __construct()
    {
        parent::__construct();
        //odbieranie inputów z angulara
        $post = file_get_contents('php://input');
        //przekazując drugi parametr na true, dekodujemy nie jako obiekt tylko tablica
        $_POST = json_decode($post, true);

        //zaladowanie modelu danych do funkcji (w tym przypadku modelu z folderu models/site w api), model ten wczytuje dane o produktach z bazy i jest dostępny globalnie z racji umieszczenia w construct
        $this->load->model('site/Orders_model');
    }


    public function create()
    {
        //pobieranie tokena z formularza aby go pozniej sprawdzic
        $token = $this->input->post( 'token' );
        //odkodowanie tokena 1 param to token, a 2 param encryption key z configu
        $this->jwt->decode( $token , config_item( 'encryption_key' ) );

        //wyciąganie payload z inputu
        $payload = $this->input->post( 'payload' );
        //odpinanie z pobierania inputu 'role', bo do realizacji zamówienia nie jest potrzebna rola uzytkownika
        unset( $payload['role'] );

        //przypisanie danych pobranych powyzej z payload do zmiennej $data
        $data = $payload;

        //wyciąganie items z inputu
        $items = $this->input->post( 'items' );
        //zakodowanie items do json, jako obiekt (nie tabelę)
        $items = json_encode( $items , JSON_FORCE_OBJECT );
        //przypisanie zmiennej items do data['items]
        $data['items'] = $items;
        //wyciąganie ceny total z inputu
        $data['total'] = $this->input->post( 'total' );


        var_dump($data);

        //tworzenie zamówienia funkcją create z modelu
        $this->Orders_model->create( $data );
    }
}
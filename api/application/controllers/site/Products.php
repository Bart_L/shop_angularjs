<?php
/**
 * Created by PhpStorm.
 * User: 7comp
 * Date: 2017-07-19
 * Time: 13:48
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    // w construct wszystkie funckje są dostępne na zewnątrz (podobnie jak rootscope w angularzejs
    public function __construct()
    {
        parent::__construct();
        //odbieranie inputów z angulara
        $post = file_get_contents('php://input');
        //przekazując drugi parametr na true, dekodujemy nie jako obiekt tylko tablica
        $_POST = json_decode($post, true);

        //zaladowanie modelu danych do funkcji (w tym przypadku modelu z folderu models/site w api), model ten wczytuje dane o produktach z bazy i jest dostępny globalnie z racji umieszczenia w construct
        $this->load->model('site/Products_model');
    }

    public function get($id = false)
    {
        //po zaladowaniu modelu, przypisujemy do zmiennej $output funkcje get modelu (nie kontrolera, tylko modelu o tej samej nazwie get)
        $output = $this->Products_model->get($id);
        //zakodowanie danych pobranych z modelu za pomocą get i przypisanych do output do JSONa
        echo json_encode( $output );
    }
}




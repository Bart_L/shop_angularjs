<?php
/**
 * Created by PhpStorm.
 * User: Bartosz
 * Date: 19.07.2017
 * Time: 19:57
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{

    public function get($id)
    {
        $this->db->where('id', $id);
        // wczytaj get produkt
        $q = $this->db->get('users');
        //końcowy rezultat to jeden wiersz z bazy
        $q = $q->row();

        //zwrocenie zawartości q
        return $q;

    }

//model dodawania nowego obiektu do bazy
    public function create($user)
    {
        //dodawanie nowego za pomocą insert - podajemy nazwe tabeli, i obiekt
        $this->db->insert('users', $user);
    }

    //funkcja odbiera w argumentach login i hasło z kontrollera
    public function login($email, $password)
    {
        //odniesienie się do bazy gdzie email równa się przekazany $email
        $this->db->where( 'email' , $email );
        //zapytanie o tablice użytkowników
        $q = $this->db->get( 'users' );
        //jezeli to zapytenie zwroci prawde, to bedzie znaczylo ze jest taki uzytkownik z takim adresem email
        $result = $q->row();


        //potrzebujemy jeszcze sprawdzic haslo


        //jezeli pole jest puste lub wpisane hasło nie równa się hasłu z serwera
        if ( empty( $result ) || $password != $result->password )
        {
            $output = false;
        }
        else
        {
            //jezeli wszystko bedzie ok, to output to beda dane użytkownika
            $output = $result;
        }
        //zwroc output taki jaki wyszedl z powyzszego warunku
        return $output;
    }

}



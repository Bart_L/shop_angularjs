<?php
/**
 * Created by PhpStorm.
 * User: 7comp
 * Date: 2017-07-19
 * Time: 13:51
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

    public function get( $id = false)
    {

        //jesli id nie jest zdefiniowane, to wyswietl wszystkie produkty
        if ( $id == false)
        {

            //funkcja pobiera tabele z bazy db - jako argument przyjmuje nazwe tabeli w bazie ; całe zapytanie przypisujemy do zmiennej query q
            //należy pamiętać by przypisać database do zmiennej libraries w folderze codeignitera config > plik autoload.php
            $q = $this->db->get('products');
            //w ten sposób mamy koncowy rezultat
            $q = $q->result();

        }
        //jezeli id jest zdefinioane to pobierz tylko dany produkt
        else
        {
            $this->db->where('id', $id);
            // wczytaj get produkt
            $q = $this->db->get('products');
            //końcowy rezultat to jeden wiersz z bazy
            $q = $q->row();

        }

        //zwrocenie zawartości q
        return $q;
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

    public function get( $id = false)
    {

        //jesli id nie jest zdefiniowane, to wyswietl wszystkie produkty
        if ( $id == false)
        {

            //funkcja pobiera tabele z bazy db - jako argument przyjmuje nazwe tabeli w bazie ; całe zapytanie przypisujemy do zmiennej query q
            //należy pamiętać by przypisać database do zmiennej libraries w folderze codeignitera config > plik autoload.php
            $q = $this->db->get('products');
            //w ten sposób mamy koncowy rezultat
            $q = $q->result();

        }
        //jezeli id jest zdefinioane to pobierz tylko dany produkt
        else
        {
            $this->db->where('id', $id);
            // wczytaj get produkt
            $q = $this->db->get('products');
            //końcowy rezultat to jeden wiersz z bazy
            $q = $q->row();

        }

        //zwrocenie zawartości q
        return $q;
    }


    //tutaj do przekazywanej wartosci w funkcji nie robimy false, poniewaz updatowany konkretny obiekt musi miec id, w przeciwnym razie nie bedzie updatowany, albo bedzie updatowane cos innego
    public function update($product)
    {
         //permanentetny zapis do bazy, ale żeby updatowac konkretny rekord musimy okreslic 'where'
        $this->db->where('id',$product['id'] );
        //updatujemy podając nazwe bazy, a jako drugi argument podajemy dane które chcemy updatowac (w tym wypadku konkretny produkt, czyli zmienna product)
        $this->db->update('products', $product);
    }

    //model dodawania nowego obiektu do bazy
    public function create($product)
    {
        //dodawanie nowego za pomocą insert - podajemy nazwe tabeli, i obiekt
        $this->db->insert('products', $product);
    }

    public function delete($product)
    {
        //zeby usunąć konkretny rekord musimy okreslic 'where'
        $this->db->where('id',$product['id'] );
        //updatujemy podając nazwe bazy, a jako drugi argument podajemy dane które chcemy updatowac (w tym wypadku konkretny produkt, czyli zmienna product)
        $this->db->delete('products', $product);
    }


}

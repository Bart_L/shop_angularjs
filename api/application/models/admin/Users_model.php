<?php
/**
 * Created by PhpStorm.
 * User: 7comp
 * Date: 2017-07-18
 * Time: 11:42
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function get( $id = false)
    {

        //jesli id nie jest zdefiniowane, to wyswietl wszystkie produkty
        if ( $id == false)
        {

            //funkcja pobiera tabele z bazy db - jako argument przyjmuje nazwe tabeli w bazie ; całe zapytanie przypisujemy do zmiennej query q
            //należy pamiętać by przypisać database do zmiennej libraries w folderze codeignitera config > plik autoload.php
            $q = $this->db->get('users');
            //w ten sposób mamy koncowy rezultat
            $q = $q->result();

        }
        //jezeli id jest zdefinioane to pobierz tylko dany produkt
        else
        {
            $this->db->where('id', $id);
            // wczytaj get produkt
            $q = $this->db->get('users');
            //końcowy rezultat to jeden wiersz z bazy
            $q = $q->row();

        }

        //zwrocenie zawartości q
        return $q;
    }


    //tutaj do przekazywanej wartosci w funkcji nie robimy false, poniewaz updatowany konkretny obiekt musi miec id, w przeciwnym razie nie bedzie updatowany, albo bedzie updatowane cos innego
    public function update($user)
    {
        //permanentetny zapis do bazy, ale żeby updatowac konkretny rekord musimy okreslic 'where'
        $this->db->where('id',$user['id'] );
        //updatujemy podając nazwe bazy, a jako drugi argument podajemy dane które chcemy updatowac (w tym wypadku konkretny produkt, czyli zmienna user)
        $this->db->update('users', $user);
    }

    //model dodawania nowego obiektu do bazy
    public function create($user)
    {
        //dodawanie nowego za pomocą insert - podajemy nazwe tabeli, i obiekt
        $this->db->insert('users', $user);
    }

    public function delete($user)
    {
        //zeby usunąć konkretny rekord musimy okreslic 'where'
        $this->db->where('id',$user['id'] );
        //updatujemy podając nazwe bazy, a jako drugi argument podajemy dane które chcemy updatowac (w tym wypadku konkretny produkt, czyli zmienna user)
        $this->db->delete('users', $user);
    }

    public function get_unique($id, $email)
    {
        $this->db->where('email', $email);

        //sprawdzamy czy id jest rozne lub jesli nie, to zmieniamy warunek i sprawdzamy czy id jest rózne od id pobranego; tworzymy zapytanie query z konkretnej tabeli users i pobieramy return wiersz row
        //jezeli zostanie pobrane tzn ze istnieje juz unikalny uzytkownik, a jezeli nie zostanie, tzn ze jest pobrany aktualnie edytowany
        !$id || $this->db->where('id !=', $id);
        $q = $this->db->get('users');

        return $q->row();
    }
}

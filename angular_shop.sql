-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 18 Lip 2017, 22:16
-- Wersja serwera: 10.1.24-MariaDB
-- Wersja PHP: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `angular_shop`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `uderId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `items` text NOT NULL,
  `total` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `orders`
--

INSERT INTO `orders` (`id`, `uderId`, `name`, `email`, `items`, `total`, `status`) VALUES
(1, 1, 'Alex', 'foo@bar.com', '', '68.42', 0),
(2, 2, 'Rose', 'foo@rose.com', '', '19.45', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `weight` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `products`
--

INSERT INTO `products` (`id`, `name`, `weight`, `description`, `price`) VALUES
(1, 'pomarańcza', '0.5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis dui sed massa porta ultrices. Nunc dolor magna, bibendum auctor placerat ac, fringilla posuere elit. In ac nunc velit. In in elementum metus. Phasellus ut egestas nibh, sed lacinia felis.', '10'),
(2, 'jabłko', '0.1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis dui sed massa porta ultrices. Nunc dolor magna, bibendum auctor placerat ac, fringilla posuere elit. In ac nunc velit. In in elementum metus. Phasellus ut egestas nibh, sed lacinia felis.', '5'),
(3, 'arbuz', '0.5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis dui sed massa porta ultrices. Nunc dolor magna, bibendum auctor placerat ac, fringilla posuere elit. In ac nunc velit. In in elementum metus. Phasellus ut egestas nibh, sed lacinia felis.', '14'),
(4, 'banan', '0.2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis dui sed massa porta ultrices. Nunc dolor magna, bibendum auctor placerat ac, fringilla posuere elit. In ac nunc velit. In in elementum metus. Phasellus ut egestas nibh, sed lacinia felis.', '3');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `passconf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `password`, `passconf`) VALUES
(1, 'Aliquet', 'aliquet.libero@Cumsociis.com', 'user', 'asd', 'asd'),
(2, 'Fletcher', 'dolor@ultrices.net', 'user', '', ''),
(3, 'Gail', 'enim.nisl.elementum@Quisqueliberolacus.com', 'user', '', ''),
(4, 'Keiko', 'porta.elit@arcuMorbi.edu', 'user', '', ''),
(5, 'Shellie', 'cursus@pretium.org', 'user', '', ''),
(6, 'Ruth', 'Sed.congue@purusNullamscelerisque.ca', 'user', '', ''),
(7, 'Ursula', 'eget.metus.In@acfacilisis.co.uk', 'user', '', ''),
(8, 'Zenaida', 'hendrerit.Donec.porttitor@vehicula.net', 'user', '', ''),
(9, 'Kasper', 'ac.tellus@massalobortisultrices.co.uk', 'user', '', ''),
(10, 'Elaine', 'sapien@feugiattelluslorem.net', 'user', '', ''),
(11, 'Callum', 'fringilla.mi@necenim.org', 'user', '', ''),
(12, 'Ramona', 'Suspendisse@MorbimetusVivamus.net', 'user', '', ''),
(14, 'Bartek', 'bartek@bartek.pl', 'user', '123', '123');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
